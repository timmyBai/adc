﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADC100.VM
{
    public class PRS006VM
    {
        // 醫令代碼
        public string ORDID { get; set; }
        // 醫令名稱
        public string ORDNME { get; set; }
        // 有效開始日期
        public string SDATE { get; set; }
        // 有效結束日期
        public string EDATE { get; set; }
        // 門住停用註記 1.門 2.住 3.門住
        public string ACTION { get; set; }
        // 售價單位
        public string BIUNT { get; set; }
        // 劑量單位
        public string PKUNT { get; set; }
    }
}
