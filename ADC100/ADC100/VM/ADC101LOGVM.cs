﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADC100.VM
{
    /// <summary>
    /// ADC 刪除藥品紀錄
    /// </summary>
    public class InsertADC101LOGVM
    {
        //ADC 機器編號
        public string ADC_NO { get; set; }
        //單位代碼/庫別
        public string STKCOD { get; set; }
        //醫令碼
        public string ORDID { get; set; }
        //藥品位置
        public string STORAGE_POS { get; set; }
        //儲位號
        public string STORAGE_NO { get; set; }
        //安全量
        public string SAFE_AMOUNT { get; set; }
        //標準量
        public string STANDARD_AMOUNT { get; set; }
        //刪除人員
        public string DELPOP { get; set; }
        //刪除日期
        public string DELDTE { get; set; }
        //刪除時間
        public string DELTME { get; set; }
    }
}
