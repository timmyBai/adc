﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADC100.VM
{
    /// <summary>
    /// 取得 ADC101 藥品
    /// </summary>
    /*public class ADC101VM
    {
        // 醫令碼
        public string ORDID { get; set; }
        // 藥櫃位置
        public string STORAGE_POS { get; set; }
        // 儲位號
        public int STORAGE_NO { get; set; }
        // 醫令名稱
        public string ORDNME { get; set; }
    }*/

    /// <summary>
    /// 新增 ADC101 藥品
    /// </summary>
    public class InsertADC101VM
    {
        //ADC 編號
        public string ADC_NO { get; set; }
        //單位代碼/庫別
        public string STKCOD { get; set; }
        //醫令碼
        public string ORDID { get; set; }
        //藥品位置
        public string STORAGE_POS { get; set; }
        //儲位號
        public string STORAGE_NO { get; set; }
        //安全量
        public string SAFE_AMOUNT { get; set; }
        //標準量
        public string STANDARD_AMOUNT { get; set; }
        //售價單位
        public string BIUNT { get; set; }
        //劑量單位
        public string PKUNT { get; set; }
        //起始建立人員
        public string SUID { get; set; }
        //起始建立日期
        public string SDTE { get; set; }
        //起始建立時間
        public string STME { get; set; }
        //更新人員
        public string UPOP { get; set; }
        //更新日期
        public string UPDTE { get; set; }
        //更新時間
        public string UPTME { get; set; }
    }

    /// <summary>
    /// 刪除 ADC101 藥品
    /// </summary>
    public class DeleteADC101VM
    {
        // 單位代碼/庫別
        public string STKCOD { get; set; }
        // 醫令碼
        public string ORDID { get; set; }
        // 藥品位置
        public string STORAGE_POS { get; set; }
        // 儲位號
        public int STORAGE_NO { get; set; }
        //安全量
        public string SAFE_AMOUNT { get; set; }
        //標準量
        public string STANDARD_AMOUNT { get; set; }
        // 更新人員
        public string UPOP { get; set; }
        // 更新日期
        public string UPDTE { get; set; }
        // 更新時間
        public string UPTME { get; set; }
    }

    /// <summary>
    /// 舊 ADC101 藥品資料
    /// </summary>
    public class oldUpdateADC101VM
    {
        // 藥品位置
        public string STORAGE_POS { get; set; }
        // 儲位號
        public int STORAGE_NO { get; set; }
        //安全量
        public string SAFE_AMOUNT { get; set; }
        //標準量
        public string STANDARD_AMOUNT { get; set; }
        // 更新人員
        public string UPOP { get; set; }
        // 更新日期
        public string UPDTE { get; set; }
        // 更新時間
        public string UPTME { get; set; }
    }

    /// <summary>
    /// 新 ADC101 藥品資料
    /// </summary>
    public class newUpdateADC101VM
    {
        // 藥品位置
        public string STORAGE_POS { get; set; }
        // 儲位號
        public int STORAGE_NO { get; set; }
        //安全量
        public string SAFE_AMOUNT { get; set; }
        //標準量
        public string STANDARD_AMOUNT { get; set; }
        // 更新人員
        public string UPOP { get; set; }
        // 更新日期
        public string UPDTE { get; set; }
        // 更新時間
        public string UPTME { get; set; }
    }
}
