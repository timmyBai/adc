﻿namespace ADC100
{
    partial class ADC_設備維護
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ADC_設備維護));
            this.txtLocationDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnDelADC100 = new System.Windows.Forms.Button();
            this.btnUpADC100 = new System.Windows.Forms.Button();
            this.btnAddADC100 = new System.Windows.Forms.Button();
            this.chkADCStatsus = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtADCNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.cboStkcod = new System.Windows.Forms.ComboBox();
            this.ctdbgADCDevice = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctdbgADCDevice)).BeginInit();
            this.SuspendLayout();
            // 
            // txtLocationDescription
            // 
            this.txtLocationDescription.BackColor = System.Drawing.Color.White;
            this.txtLocationDescription.Location = new System.Drawing.Point(483, 54);
            this.txtLocationDescription.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.txtLocationDescription.MaxLength = 30;
            this.txtLocationDescription.Name = "txtLocationDescription";
            this.txtLocationDescription.Size = new System.Drawing.Size(220, 27);
            this.txtLocationDescription.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(387, 59);
            this.label3.Margin = new System.Windows.Forms.Padding(10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "位置描述:";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(348, 94);
            this.btnClear.Margin = new System.Windows.Forms.Padding(5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Padding = new System.Windows.Forms.Padding(5);
            this.btnClear.Size = new System.Drawing.Size(100, 40);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "清除";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDelADC100
            // 
            this.btnDelADC100.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(34)))), ((int)(((byte)(68)))));
            this.btnDelADC100.FlatAppearance.BorderSize = 0;
            this.btnDelADC100.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelADC100.ForeColor = System.Drawing.Color.White;
            this.btnDelADC100.Location = new System.Drawing.Point(238, 94);
            this.btnDelADC100.Margin = new System.Windows.Forms.Padding(5);
            this.btnDelADC100.Name = "btnDelADC100";
            this.btnDelADC100.Padding = new System.Windows.Forms.Padding(5);
            this.btnDelADC100.Size = new System.Drawing.Size(100, 40);
            this.btnDelADC100.TabIndex = 7;
            this.btnDelADC100.Text = "刪除";
            this.btnDelADC100.UseVisualStyleBackColor = false;
            this.btnDelADC100.Click += new System.EventHandler(this.btnDelADC100_Click);
            // 
            // btnUpADC100
            // 
            this.btnUpADC100.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(84)))), ((int)(((byte)(124)))));
            this.btnUpADC100.FlatAppearance.BorderSize = 0;
            this.btnUpADC100.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpADC100.ForeColor = System.Drawing.Color.White;
            this.btnUpADC100.Location = new System.Drawing.Point(123, 94);
            this.btnUpADC100.Margin = new System.Windows.Forms.Padding(5);
            this.btnUpADC100.Name = "btnUpADC100";
            this.btnUpADC100.Padding = new System.Windows.Forms.Padding(5);
            this.btnUpADC100.Size = new System.Drawing.Size(100, 40);
            this.btnUpADC100.TabIndex = 6;
            this.btnUpADC100.Text = "修改";
            this.btnUpADC100.UseVisualStyleBackColor = false;
            this.btnUpADC100.Click += new System.EventHandler(this.btnUpADC100_Click);
            // 
            // btnAddADC100
            // 
            this.btnAddADC100.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.btnAddADC100.FlatAppearance.BorderSize = 0;
            this.btnAddADC100.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddADC100.ForeColor = System.Drawing.Color.White;
            this.btnAddADC100.Location = new System.Drawing.Point(13, 94);
            this.btnAddADC100.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddADC100.Name = "btnAddADC100";
            this.btnAddADC100.Padding = new System.Windows.Forms.Padding(5);
            this.btnAddADC100.Size = new System.Drawing.Size(100, 40);
            this.btnAddADC100.TabIndex = 5;
            this.btnAddADC100.Text = "新增";
            this.btnAddADC100.UseVisualStyleBackColor = false;
            this.btnAddADC100.Click += new System.EventHandler(this.btnAddADC100_Click);
            // 
            // chkADCStatsus
            // 
            this.chkADCStatsus.AutoSize = true;
            this.chkADCStatsus.Location = new System.Drawing.Point(299, 58);
            this.chkADCStatsus.Name = "chkADCStatsus";
            this.chkADCStatsus.Size = new System.Drawing.Size(59, 20);
            this.chkADCStatsus.TabIndex = 3;
            this.chkADCStatsus.Text = "啟用";
            this.chkADCStatsus.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(408, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "護理站:";
            // 
            // txtADCNo
            // 
            this.txtADCNo.BackColor = System.Drawing.Color.White;
            this.txtADCNo.Location = new System.Drawing.Point(138, 10);
            this.txtADCNo.Margin = new System.Windows.Forms.Padding(10);
            this.txtADCNo.MaxLength = 3;
            this.txtADCNo.Name = "txtADCNo";
            this.txtADCNo.Size = new System.Drawing.Size(220, 27);
            this.txtADCNo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(10, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "ADC設備編號:";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnClose.Location = new System.Drawing.Point(669, 498);
            this.btnClose.Margin = new System.Windows.Forms.Padding(10);
            this.btnClose.Name = "btnClose";
            this.btnClose.Padding = new System.Windows.Forms.Padding(5);
            this.btnClose.Size = new System.Drawing.Size(100, 40);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "離開";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.cboStkcod);
            this.panelMenu.Controls.Add(this.btnClear);
            this.panelMenu.Controls.Add(this.txtLocationDescription);
            this.panelMenu.Controls.Add(this.btnDelADC100);
            this.panelMenu.Controls.Add(this.label3);
            this.panelMenu.Controls.Add(this.btnUpADC100);
            this.panelMenu.Controls.Add(this.label1);
            this.panelMenu.Controls.Add(this.btnAddADC100);
            this.panelMenu.Controls.Add(this.txtADCNo);
            this.panelMenu.Controls.Add(this.label2);
            this.panelMenu.Controls.Add(this.chkADCStatsus);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenu.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(784, 148);
            this.panelMenu.TabIndex = 5;
            // 
            // cboStkcod
            // 
            this.cboStkcod.BackColor = System.Drawing.Color.White;
            this.cboStkcod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStkcod.FormattingEnabled = true;
            this.cboStkcod.Items.AddRange(new object[] {
            "請選擇"});
            this.cboStkcod.Location = new System.Drawing.Point(483, 15);
            this.cboStkcod.Margin = new System.Windows.Forms.Padding(10);
            this.cboStkcod.Name = "cboStkcod";
            this.cboStkcod.Size = new System.Drawing.Size(220, 24);
            this.cboStkcod.TabIndex = 2;
            // 
            // ctdbgADCDevice
            // 
            this.ctdbgADCDevice.AllowUpdate = false;
            this.ctdbgADCDevice.AllowUpdateOnBlur = false;
            this.ctdbgADCDevice.AlternatingRows = true;
            this.ctdbgADCDevice.CaptionHeight = 19;
            this.ctdbgADCDevice.EmptyRows = true;
            this.ctdbgADCDevice.FlatStyle = C1.Win.C1TrueDBGrid.FlatModeEnum.Flat;
            this.ctdbgADCDevice.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ctdbgADCDevice.GroupByCaption = "Drag a column header here to group by that column";
            this.ctdbgADCDevice.Images.Add(((System.Drawing.Image)(resources.GetObject("ctdbgADCDevice.Images"))));
            this.ctdbgADCDevice.Location = new System.Drawing.Point(14, 161);
            this.ctdbgADCDevice.Margin = new System.Windows.Forms.Padding(5, 10, 5, 10);
            this.ctdbgADCDevice.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow;
            this.ctdbgADCDevice.Name = "ctdbgADCDevice";
            this.ctdbgADCDevice.PreviewInfo.Location = new System.Drawing.Point(0, 0);
            this.ctdbgADCDevice.PreviewInfo.Size = new System.Drawing.Size(0, 0);
            this.ctdbgADCDevice.PreviewInfo.ZoomFactor = 75D;
            this.ctdbgADCDevice.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("ctdbgADCDevice.PrintInfo.PageSettings")));
            this.ctdbgADCDevice.RowHeight = 24;
            this.ctdbgADCDevice.Size = new System.Drawing.Size(756, 317);
            this.ctdbgADCDevice.TabIndex = 8;
            this.ctdbgADCDevice.Text = "ADC 設備清單";
            this.ctdbgADCDevice.ButtonClick += new C1.Win.C1TrueDBGrid.ColEventHandler(this.ctdbgADCDevice_ButtonClick);
            this.ctdbgADCDevice.DoubleClick += new System.EventHandler(this.ctdbgADCDevice_DoubleClick);
            this.ctdbgADCDevice.PropBag = resources.GetString("ctdbgADCDevice.PropBag");
            // 
            // ADC_設備維護
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(784, 556);
            this.ControlBox = false;
            this.Controls.Add(this.ctdbgADCDevice);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "ADC_設備維護";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADC_設備維護";
            this.Load += new System.EventHandler(this.ADC_設備維護_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ADC_設備維護_KeyDown);
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctdbgADCDevice)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtADCNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkADCStatsus;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnDelADC100;
        private System.Windows.Forms.Button btnUpADC100;
        private System.Windows.Forms.Button btnAddADC100;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLocationDescription;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.ComboBox cboStkcod;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid ctdbgADCDevice;
    }
}