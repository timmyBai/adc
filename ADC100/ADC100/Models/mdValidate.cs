﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADC100.Models
{
    /// <summary>
    /// 驗證 ADC100
    /// </summary>
    class mdADC100Validate
    {
        /// <summary>
        /// 驗證 ADC 資料新增、修改資料
        /// </summary>
        /// <param name="adcNo">ADC 編號</param>
        /// <param name="stationId">單位代碼</param>
        /// <param name="locationDescription">位置描述</param>
        /// <returns></returns>
        public string VerifyADCDriveData(string adcNo, string stkcod, string locationDescription)
        {
            string result = string.Empty;

            if (adcNo.Length == 0 || adcNo.Length > 3)
            {
                result = "ADC 設備編號輸入介於 1 至 3 字元";
            }

            else if (stkcod == "請選擇")
            {
                result = "未選擇護理站別";
            }

            else if (locationDescription.Length == 0 || locationDescription.Length > 30)
            {
                result = "位置描述輸入介於 1 至 30 字元";
            }

            return result;
        }
    }

    /// <summary>
    /// 驗證 ADC101
    /// </summary>
    class mdADC101Validate
    {
        public string VerifyADC101Data(string ordid, string storeagePos, string storeageId)
        {
            string result = string.Empty;

            if (ordid.Length == 0)
            {
                result = "未輸入醫令";
            }
            else if (storeagePos.Length == 0)
            {
                result = "未輸入放置藥櫃";
            }
            else if (storeageId.Length == 0)
            {
                result = "未輸入儲未號";
            }

            return result;
        }
    }
}
