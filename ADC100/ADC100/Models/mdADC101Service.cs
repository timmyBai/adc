﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

using Dapper;
using KTDB;
using ADC100.VM;

namespace ADC100.Models
{
    class mdADC101Service
    {
        private KTConnectionControler KTCC;

        public string sqlStr = string.Empty;

        public mdADC101Service(KTConnectionControler _KTCC)
        {
            KTCC = _KTCC;
        }

        /// <summary>
        /// 取得 ADC 藥櫃藥品清單
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public DataTable GetADC100DrugList(string adcNo, string stkcod)
        {
            DataTable dtADC101 = new DataTable("ADC101");
            sqlStr = @"SELECT ADC101.ORDID,
                              ADC101.STORAGE_POS,
                              ADC101.STORAGE_NO,
                              ADC101.SAFE_AMOUNT,
                              ADC101.STANDARD_AMOUNT,
                              ADC101.BIUNT,
                              ADC101.PKUNT,
                              PRS006.ORDNME
                         FROM ADC101
                              LEFT OUTER JOIN ADC100 ON ( ADC100.ADC_NO = ADC101.ADC_NO )
                              LEFT OUTER JOIN ( SELECT DPTID,
                                                       FNAME
                                                  FROM PRS002
                                                    WHERE     DTFLG      = '2'
                                                          AND VISIBLEFLG = 'Y' ) PRS002
                                  ON ( PRS002.DPTID = ADC101.STKCOD )
                              LEFT OUTER JOIN ( SELECT ORDID,
                                                       ORDNME,
                                                       SDATE,
                                                       EDATE,
                                                       ACTION
                                                  FROM PRS006
                                                    WHERE NO = '0' ) PRS006
                                  ON ( PRS006.ORDID = ADC101.ORDID ) WHERE     ADC101.ADC_NO = :ADC_NO
                                                                           AND ADC101.STKCOD = :STKCOD
                                                                         ORDER BY ORDID ASC
            ";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            cmd.Parameters("ADC_NO", adcNo);
            cmd.Parameters("STKCOD", stkcod);
            KTCC.DoSql(cmd, dtADC101);

            return dtADC101;
        }

        /// <summary>
        /// 檢查藥品
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="adcNo"></param>
        /// <param name="stkcod"></param>
        /// <param name="ordid"></param>
        /// <returns></returns>
        public T InspactADC101<T>(string adcNo, string stkcod, string ordid)
        {
            sqlStr = @"SELECT STORAGE_POS,
                              STORAGE_NO,
                              SAFE_AMOUNT,
                              STANDARD_AMOUNT,
                              UPOP,
                              UPDTE,
                              UPTME
                       FROM ADC101 WHERE     ADC_NO = :ADC_NO
                                         AND STKCOD = :STKCOD
                                         AND ORDID  = :ORDID
            ";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            cmd.Parameters("ADC_NO", adcNo);
            cmd.Parameters("STKCOD", stkcod);
            cmd.Parameters("ORDID", ordid);
            var result = KTCC.DoSql<T>(cmd).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增 ADC101 藥品
        /// </summary>
        /// <param name="insertADC100"></param>
        public void InsertADC101(InsertADC101VM insertADC100)
        {
            DynamicParameters param;
            KTCC.CreateInsertSql(insertADC100, "ADC101", out sqlStr, out param);
            KTCC.DoSql(sqlStr, param);
        }

        /// <summary>
        /// 更新 ADC101 藥品資料
        /// </summary>
        /// <param name="adcNo"></param>
        /// <param name="stkcod"></param>
        /// <param name="ordid"></param>
        /// <param name="newObj"></param>
        /// <param name="oldObj"></param>
        public void UpdateADC101(string adcNo, string stkcod, string ordid, newUpdateADC101VM newObj, oldUpdateADC101VM oldObj)
        {
            DynamicParameters param;
            string whereKey = " WHERE ADC_NO = :ADC_NO AND STKCOD = :STKCOD AND ORDID = :ORDID";
            object paramKey = new { ADC_NO = adcNo, STKCOD = stkcod, ORDID = ordid };
            KTCC.CreateUpdateSql(newObj, oldObj, "ADC101", out sqlStr, out param, whereKey, paramKey);
            KTCC.DoSql(sqlStr, param);
        }

        /// <summary>
        /// 刪除 ADC101 藥品資料
        /// </summary>
        /// <param name="adcNo"></param>
        /// <param name="stkcod"></param>
        /// <param name="ordid"></param>
        public void DeleteADC101(string adcNo, string stkcod, string ordid)
        {
            sqlStr = @"DELETE FROM ADC101 WHERE     ADC_NO = :ADC_NO
                                                AND STKCOD = :STKCOD
                                                AND ORDID  = :ORDID
            ";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            cmd.Parameters("ADC_NO", adcNo);
            cmd.Parameters("STKCOD", stkcod);
            cmd.Parameters("ORDID", ordid);
            KTCC.DoSql(cmd);
        }
    }
}
