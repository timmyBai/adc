﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADC102.VM
{
    /// <summary>
    /// 抓取位待審清單
    /// </summary>
    /*public class ADC102VM
    {
        //病歷號
        public string CRTNO { get; set; }
        //流水號
        public string ACTNO { get; set; }
        //來源
        public string SRFLG { get; set; }
        //醫令碼
        public string ORDID { get; set; }
        //醫令名稱
        public string ORDNME { get; set; }
        //處方日期
        public string ORDDTE { get; set; }
        //處方時間
        public string ORDTME { get; set; }
        //單位/庫存
        public string STKCOD { get; set; }
        //序號
        public string SCRNO { get; set; }
        //床號/診號
        public string VSCLIN { get; set; }
        //劑量
        public int DOSAGE { get; set; }
        //醫師姓名
        public string OPNAME { get; set; }
        //故意綁定欄位 '同意'
        public string AGREE { get; set; }
        //故意綁定欄位 '不同意'
        public string DISAGREE { get; set; }
    }
    */

    /// <summary>
    /// ADC102 key
    /// </summary>
    public class ADC102Key
    {
        public string CRTNO { get; set; }
        public string ACTNO { get; set; }
        public string SRFLG { get; set; }
        public string ORDID { get; set; }
        public string ORDDTE { get; set; }
        public string ORDTME { get; set; }
        public string SCRNO { get; set; }
        public string STKCOD { get; set; }
    }

    /// <summary>
    /// 舊 ADC102 資料
    /// </summary>
    public class oldUpdateADC102VM
    {
        public string REASON { get; set; }
        public string AGREEUPOP { get; set; }
        //同意/不同意 註記
        public string AGREEFLG { get; set; }
        //同意/不同意 日期
        public string AGREEDTE { get; set; }
        //同意/不同意 時間
        public string AGREETME { get; set; }
    }

    /// <summary>
    /// 新 ADC102 資料
    /// </summary>
    public class newUpdateADC102VM
    {
        public string REASON { get; set; }
        public string AGREEUPOP { get; set; }
        //同意/不同意 註記
        public string AGREEFLG { get; set; }
        //同意/不同意 日期
        public string AGREEDTE { get; set; }
        //同意/不同意 時間
        public string AGREETME { get; set; }
    }

    public class ADCSupport
    {
        //處方日期
        public string ORDDTE { get; set; }
        //處方時間
        public string ORDTME { get; set; }
        //單位/庫別
        public string STKCOD { get; set; }
        //病歷號
        public string CRTNO { get; set; }
        //性別
        public string PTSEX { get; set; }
        //患者名稱
        public string PTNAME { get; set; }
        //生日
        public string BRDTE { get; set; }
        //床號/診號
        public string VSLIN { get; set; }
        //流水號
        public string ACTNO { get; set; }
        //來源
        public string SRFLG { get; set; }
        //序號
        public string SCRNO { get; set; }
        //醫令碼
        public string ORDID { get; set; }
        //劑量
        public string DOSAGE { get; set; }
        //售價單位
        public string PKUNT { get; set; }
        //處方量
        public string ORDAMT { get; set; }
        //劑量單位
        public string BIUNT { get; set; }
        //頻率
        public string FREQ { get; set; }
        //同意人員
        public string AGREEUPOP { get; set; }
    }

}