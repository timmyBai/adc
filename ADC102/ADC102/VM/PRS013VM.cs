﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADC102.VM
{
    //抓取患者基本資料
    public class PRS013BaseVM
    {
        //病歷號
        public string CRTNO { get; set; }
        //患者姓名
        public string PTNAME { get; set; }
        //性別
        public string PTSEX { get; set; }
        //生日
        public string BRDTE { get; set; }

    }
}
