﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KTDB;
using ADC102.VM;

namespace ADC102.Models
{
    public class mdPRS014Service
    {
        private KTConnectionControler KTCC;

        public string sqlStr = string.Empty;

        public mdPRS014Service(KTConnectionControler _KTCC)
        {
            KTCC = _KTCC;
        }

        /// <summary>
        /// 尋找掛號資訊
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="crtno"></param>
        /// <param name="actno"></param>
        /// <returns></returns>
        public T GetPrs014RegisterInfo<T>(string crtno, string actno)
        {
            sqlStr = @"SELECT CRTNO,
                              ACTNO,
                              PTNAME,
                              VSDR,
                              VSAPN,
                              VSDTE,
                              BRDTE,
                              VSDPT,
                              DPTNAME,
                              CASE VSAPN
                                  WHEN '1' THEN '早上'
                                  WHEN '2' THEN '下午'
                                           ELSE '晚上'
                              END AS VSAPN,
                              CASE PTSEX
                                  WHEN '1' THEN '男'
                                           ELSE '女'
                              END AS PTSEX
                         FROM PRS014
                        WHERE CRTNO = :CRTNO AND ACTNO = :ACTNO
            ";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            cmd.Parameters("CRTNO", crtno);
            cmd.Parameters("ACTNO", actno);
            var result = KTCC.DoSql<T>(cmd).SingleOrDefault();

            return result;
        }
    }
}
