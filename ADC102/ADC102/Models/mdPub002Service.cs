﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KTDB;

namespace ADC102.Models
{
    public class mdPub002Service
    {
        private KTConnectionControler KTCC;

        public string sqlStr;

        public mdPub002Service(KTConnectionControler _KTCC)
        {
            KTCC = _KTCC;
        }

        public IEnumerable<T> GetPub002Stkcod<T>()
        {
            sqlStr = "SELECT CODE AS DPTID, CNAME AS FNAME FROM PUB002 WHERE DTATYP='GAD' ";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            var result = KTCC.DoSql<T>(cmd).ToList();

            return result;
        }
    }
}
