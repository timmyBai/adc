﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADC102.Models
{
    class mdOtherFun
    {
        //轉換來源字串中文轉數字
        public string ConvertCheineseToNumberSrflg(string strSrflg)
        {
            string result = "";

            if (strSrflg == "門診")
                result = "1";

            if (strSrflg == "急診")
                result = "2";

            if (strSrflg == "住院")
                result = "3";

            return result;
        }

        //轉換來源字串數字轉中文
        public string ConvertNumberToChineseSrflg(string strSrflg)
        {
            string result = "";

            if (strSrflg == "1")
                result = "門診";

            if (strSrflg == "2")
                result = "急診";

            if (strSrflg == "3")
                result = "住院";

            return result;
        }
    }
}
